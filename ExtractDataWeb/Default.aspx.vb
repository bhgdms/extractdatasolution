﻿Imports System.IO
Imports System.Web.Services
Imports System.Xml
'Imports wordDoc = Microsoft.Office.Interop.Word
Imports excelDoc = Microsoft.Office.Interop.Excel


Public Class _Default
    Inherits Page

    Public Shared Function GetRootPath() As String

        Dim _ApplicationPath As String = HttpContext.Current.Request.UrlReferrer.ToString()
        Dim pathToReturn As StringBuilder = New StringBuilder(_ApplicationPath)

        If Not _ApplicationPath.EndsWith("/") Then
            pathToReturn.Append("/")
        End If

        Return pathToReturn.ToString()

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

    End Sub

    <WebMethod()>
    Public Shared Function UpdateDataAsPerTemplate(fileName As String, templateId As String) As String
        Try
            Dim filePath = HttpContext.Current.Server.MapPath("~/Order-PDF/")
            Dim pdfFile As String = filePath + fileName
            Dim excelFile As String = Path.ChangeExtension(pdfFile, ".xls")
            Dim xmlFile As String = Path.ChangeExtension(pdfFile, ".xml")
            Dim xml As String = Nothing
            Dim generatedFile As String = String.Empty

            Dim f As New SautinSoft.PdfFocus()

            f.OpenPdf(pdfFile)

            If f.PageCount > 0 Then
                xml = f.ToXml()
                f.ClosePdf()

                Dim dtFromPDF As New DataTable
                Dim dtDestination As New DataTable

                If Not String.IsNullOrEmpty(xml) Then
                    File.WriteAllText(xmlFile, xml)
                    'System.Diagnostics.Process.Start(xmlFile)

                    Dim fs As New FileStream(xmlFile, FileMode.Open)
                    Dim xr As New XmlTextReader(fs)

                    Dim ds As New DataSet
                    ds.ReadXml(xr)
                    dtFromPDF = ds.Tables(1)

                    fs.Close()
                    If System.IO.File.Exists(xmlFile) Then
                        System.IO.File.Delete(xmlFile)
                    End If

                    dtDestination = ExtractDataIntoDataTable(dtFromPDF)
                    LogException("destination rows count : " + dtDestination.Rows.Count.ToString())
                    generatedFile = CompareAndUpdateData(dtDestination, templateId, Path.GetFileNameWithoutExtension(fileName))
                    'CreateExcelFile(dtDestination) 

                    Return "SUCCESS#" + generatedFile
                End If
            End If
        Catch ex As Exception
            LogException(ex.ToString())
            Return "ERROR#" + ex.StackTrace.ToString()
        End Try

    End Function

    Private Shared Function ExtractDataIntoDataTable(ByVal dtFromPDF As DataTable) As DataTable
        Dim dtDestination As New DataTable
        Dim col2Added As Boolean = False
        Dim col3Added As Boolean = False
        Dim col4Added As Boolean = False
        Dim col5Added As Boolean = False
        Dim col6Added As Boolean = False

        Dim prevRowId As Integer = 0
        Dim dtNewRow As DataRow

        For index As Integer = 0 To dtFromPDF.Rows.Count - 1
            If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("row_id").ToString()) Then
                Dim rowId As Integer = dtFromPDF.Rows(index)("row_id").ToString()

                If index = 0 Then
                    prevRowId = 0
                Else
                    prevRowId = dtFromPDF.Rows(index - 1)("row_id").ToString()
                End If


                If rowId = 20 Then
                    Console.WriteLine(dtFromPDF.Rows(index)("cell_Text").ToString())
                    If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) Then
                        dtDestination.Columns.Add(dtFromPDF.Rows(index)("cell_Text").ToString())
                    End If
                ElseIf rowId > 20 And rowId < 95 Then

                    If prevRowId <> rowId Then
                        dtNewRow = dtDestination.NewRow()
                        col2Added = False
                        col3Added = False
                        col4Added = False
                        col5Added = False
                        col6Added = False

                        If dtFromPDF.Rows(index)("colspan").ToString() = "2" Then
                            dtNewRow(dtDestination.Columns(0).ColumnName) = ""
                        Else
                            If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) Then
                                dtNewRow(dtDestination.Columns(0).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            End If
                        End If
                    Else

                        If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col2Added = False Then
                            dtNewRow(dtDestination.Columns(1).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col2Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col3Added = False Then
                            dtNewRow(dtDestination.Columns(2).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col3Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col4Added = False Then
                            dtNewRow(dtDestination.Columns(3).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col4Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col5Added = False Then
                            dtNewRow(dtDestination.Columns(4).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col5Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col6Added = False Then
                            dtNewRow(dtDestination.Columns(5).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col6Added = True
                        End If

                        If col2Added AndAlso col3Added AndAlso col4Added AndAlso col5Added AndAlso col6Added Then
                            dtDestination.Rows.Add(dtNewRow)
                            col2Added = False
                            col3Added = False
                            col4Added = False
                            col5Added = False
                            col6Added = False
                        End If
                    End If

                End If
            End If

        Next

        Dim filter As String = "Item <> 'Please note that prices exclude VAT'"
        Dim FilteredRows As DataRow() = dtDestination.Select(filter)
        dtDestination = FilteredRows.CopyToDataTable()

        Return dtDestination
    End Function

    Private Shared Function CompareAndUpdateData(ByVal dt As DataTable, ByVal templateId As String, ByVal pdfFileName As String) As String

        LogException("Reading excel app : ")
        Dim xlApp As excelDoc.Application = New excelDoc.Application()
        LogException("Reading finished excel app : ")
        Dim xlWorkBook As excelDoc.Workbook
        Dim xlWorkSheet As excelDoc.Worksheet
        Dim templatePath As String = HttpContext.Current.Server.MapPath("~/Template-XLS/")
        Dim destinationFilePath As String = HttpContext.Current.Server.MapPath("~/Order-XLS/")
        Dim rootPath As String = GetRootPath() + "Order-XLS/"

        If xlApp Is Nothing Then
            Console.WriteLine("Excel is not properly installed!!")
            Return String.Empty
        Else

            If templateId = "1" Then
                xlWorkBook = xlApp.Workbooks.Open(templatePath + "1-CostaTemplate.xlsx")
            ElseIf templateId = "2" Then
                xlWorkBook = xlApp.Workbooks.Open(templatePath + "2-CostaTemplate.xlsx")
            ElseIf templateId = "3" Then
                xlWorkBook = xlApp.Workbooks.Open(templatePath + "3-CostaTemplate.xlsx")
            End If
            'xlWorkBook = xlApp.Workbooks.Open("D:\Research\281-CON1.xlsx")
            xlWorkSheet = xlWorkBook.Sheets(1)
            Dim iRow As Integer = 0
            Dim iCol As Integer = 0

            For iRow = 2 To xlWorkSheet.Rows.Count
                Dim destCode As String = xlWorkSheet.Cells(iRow, 1).value
                Dim destQuntity As String = xlWorkSheet.Cells(iRow, 3).value

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim quant As Integer
                    Dim sourceCode As String = dt.Rows(i)(0).ToString()
                    If Not String.IsNullOrEmpty(sourceCode) Then
                        If sourceCode = destCode Then
                            quant = CInt(dt.Rows(i)(2).ToString())
                            xlWorkSheet.Cells(iRow, 3).value = quant
                        End If
                    End If
                Next

                If Trim(xlWorkSheet.Cells(iRow, 1).value) = "" Then
                    Exit For        ' All data has been read, exit from excel and save it.
                End If

            Next

        End If

        Dim misValue As Object = System.Reflection.Missing.Value
        Dim generatedFileName As String = destinationFilePath + pdfFileName + "-" + Date.Now.ToString("yyyy.MM.dd-hh-mm-tt") + ".xls"
        Dim generatedFileUrl As String = rootPath + pdfFileName + "-" + Date.Now.ToString("yyyy.MM.dd-hh-mm-tt") + ".xls"
        xlWorkBook.SaveAs(generatedFileName, excelDoc.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
                                            excelDoc.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)

        xlApp.Quit()
        ReleaseObject(xlApp)
        ReleaseObject(xlWorkBook)
        ReleaseObject(xlWorkSheet)

        Return generatedFileUrl
    End Function

    Private Shared Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Public Shared Sub LogException(ByVal errorMessage As String)
        Dim traceWriter As System.Diagnostics.TextWriterTraceListener = Nothing
        Dim stringBuilder As System.Text.StringBuilder = Nothing
        Dim page As Page = Nothing

        Try
            stringBuilder = New System.Text.StringBuilder()
            page = New Page()
            stringBuilder.Append(page.Server.MapPath("~/Log"))
            stringBuilder.Append("\")
            stringBuilder.Append(DateTime.Now.Year.ToString())
            stringBuilder.Append("\")
            stringBuilder.Append(DateTime.Now.Month.ToString())
            stringBuilder.Append("\")
            stringBuilder.Append(DateTime.Now.Day.ToString())

            If (Not Directory.Exists(stringBuilder.ToString())) Then
                Directory.CreateDirectory(stringBuilder.ToString())
            End If

            stringBuilder.Append("\Error.log")
            traceWriter = New System.Diagnostics.TextWriterTraceListener(stringBuilder.ToString())
            stringBuilder = New System.Text.StringBuilder()

            stringBuilder.Append(vbCrLf)
            stringBuilder.Append("Date : ")
            stringBuilder.Append(DateTime.Now.ToString())

            stringBuilder.Append(vbCrLf)
            stringBuilder.Append("Error : ")
            stringBuilder.Append(errorMessage)

            stringBuilder.Append(vbCrLf)
            traceWriter.WriteLine(stringBuilder.ToString())
        Catch
        Finally

            If (traceWriter IsNot Nothing) Then
                traceWriter.Close()
                traceWriter.Dispose()
            End If

            stringBuilder = Nothing
        End Try
    End Sub
End Class