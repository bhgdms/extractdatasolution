﻿Imports System.Web
Imports System.Web.Services
Imports System.IO

Public Class duploader
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Try
            context.Response.ContentType = "text/plain"
            Dim FileData As String = String.Empty
            If context.Request.Files.Count > 0 Then
                Dim postedFiles As HttpFileCollection = context.Request.Files
                Dim savepath As String = context.Server.MapPath("~/Order-PDF")

                If Not Directory.Exists(savepath) Then Directory.CreateDirectory(savepath)
                For i As Integer = 0 To postedFiles.Count - 1
                    With postedFiles(i)
                        Dim fileName$ = Path.GetFileName(.FileName)
                        .SaveAs(savepath + "\" + fileName)
                        FileData = fileName
                    End With
                Next
                context.Response.Write(FileData)
                Return
            End If
            context.Response.Write("NO_FILES")
        Catch ex As Exception
            context.Response.Write("FAILED")
        End Try

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class