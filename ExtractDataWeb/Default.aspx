﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="ExtractDataWeb._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .template {
            position: relative;
            display: block;
            width: 400px;
            height: 35px;
            margin: 0 0 15px 0;
            background: #fefefe;
            border: 1px solid #cecece;
            font-size: 12px;
            font-family: sans-serif;
            color: #888;
            border-radius: 4px;
            cursor: pointer;
            overflow: hidden;
        } 
        .ajax-loader {
            /*visibility: hidden;*/
            /*background-color: rgba(255,255,255,0.7);*/
            position: absolute;
            z-index: +100 !important;
            width: 78%;
            height: 100%;
        }

        .ajax-loader img {
            /*position: relative;
            top: 50%;
            left: 50%;*/
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    
    <div class="jumbotron">
        <div class="ajax-loader">
          <img src="images/ajax-loader.gif" class="img-responsive" />
        </div>
        <h2>PDF Extractor</h2>
        <hr />
        <p class="lead">Select Client</p> 
        <asp:DropDownList ID="ddlTemplate" CssClass="template" runat="server" AutoPostBack="true">
            <asp:ListItem Text="Costa" Value="1" Selected="True">Costa</asp:ListItem>
            <asp:ListItem Text="Costa2" Value="2">Costa 2</asp:ListItem>
            <asp:ListItem Text="Costa3" Value="3">Costa 3</asp:ListItem>
        </asp:DropDownList>
        
        <p class="lead">Select Order file</p> 
        <asp:FileUpload ID="flDocument" runat="server" AllowMultiple="false" />        
        <input id="btnSubmit" type="submit" value="Execute" class="btn-custom green">
        <hr />
        <div class="w3-padding w3-red errorMessage" id="dvErrMsg">
            
        </div>
    </div>
    
    <script>
        function showLoader() {
            $('.ajax-loader').show();
        }

        function hideLoader() {
            setTimeout(function () {
                $('.ajax-loader').hide();
            }, 500);
        }


        $(document).ready(function () {
            $('#btnSubmit').hide();
            $('.errorMessage').hide(); 
            hideLoader();
            var fileName; 

            $(document).ajaxStart(function () {
                showLoader();
            });
            $(document).ajaxComplete(function () {
                hideLoader();
            });

            $(".ajax-loader").bind("ajaxStart", function () {
                $(this).show();
            }).bind("ajaxStop", function () {
                $(this).hide();
            });

            $('#<%=flDocument.ClientID%>').filer({
                showThumbs: true, 
                extensions: ["pdf"], 
                addMore: false,
                limit:1,
                allowDuplicates: false,
                onSelect: function () { 
                    $('#btnSubmit').show();
                },
                onEmpty: function () { 
                    $('#btnSubmit').hide();
                    $('.errorMessage').hide();
                },
                uploadFile: {
                    url: "../duploader.ashx", 
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    beforeSend: function () { },
                    success: function (data, el) {
                        fileName = data;
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                            $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                        $('#<%=flDocument.ClientID%>').css('disabled', true);
                    },
                    error: function (el) {
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                            $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    },
                    statusCode: null,
                    onProgress: null,
                    onComplete: null
                }, 
            });   

            $('#btnSubmit').click(function (e) {
                var template = $('#<%=ddlTemplate.ClientID%> :selected').val();  
                showLoader();

                $.ajax({
                    type: "POST",
                    url: 'default.aspx/UpdateDataAsPerTemplate',
                    data: JSON.stringify({ fileName: fileName, templateId: template }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    beforeSend: function () { showLoader(); },
                    success: function (response) {
                        var result = response.d.split('#');
                        if (result[0] == 'SUCCESS') {
                            $('.errorMessage').hide(); 
                            if (result[1] != "") {
                                e.preventDefault();
                                $('.errorMessage').show();
                                $('.errorMessage').html("Generated file :" + result[1]);
                                window.location.href = result[1]; 
                                //hideLoader();
                                
                            }
                            else {
                                $('.errorMessage').show();
                                $('.errorMessage').html("file is not generated"); 
                                //hideLoader();
                            }
                        }
                        else {
                            e.preventDefault();
                            e.stopPropagation();
                            $('.errorMessage').show();     
                            console.log(response.d); 
                            //hideLoader();
                            $('.errorMessage').html("<div style='font-weight:bold;'><span>There is some error in updating data. Error may happened due to following reasons. </span><br /><ul><li>Template is mismatched with uploaded file.</li><li>Uploaded file is not a valid or expected format.</li><li>Content is not received as expected from uploaded file. </li><li>" + response.d + "</li></ul></div>");
                        }
                    },                         
                    error: function (response) {
                        $('.errorMessage').html(response.d); 
                        //hideLoader();
                    } 
                }); 
            })

        });
    </script>
    
</asp:Content>
