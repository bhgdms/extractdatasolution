﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPDF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExecute = New System.Windows.Forms.Button()
        Me.btnSelectSingleOrder = New System.Windows.Forms.Button()
        Me.txtSelectedFile = New System.Windows.Forms.TextBox()
        Me.cmbClient1 = New System.Windows.Forms.ComboBox()
        Me.lblClient1 = New System.Windows.Forms.Label()
        Me.fileDailog1 = New System.Windows.Forms.OpenFileDialog()
        Me.rdSingle = New System.Windows.Forms.RadioButton()
        Me.rdMultiple = New System.Windows.Forms.RadioButton()
        Me.grpOption = New System.Windows.Forms.GroupBox()
        Me.grpSingle = New System.Windows.Forms.GroupBox()
        Me.grpMultiple = New System.Windows.Forms.GroupBox()
        Me.lblFileCount = New System.Windows.Forms.Label()
        Me.lstFiles = New System.Windows.Forms.ListBox()
        Me.cmbClient2 = New System.Windows.Forms.ComboBox()
        Me.lblClient2 = New System.Windows.Forms.Label()
        Me.btnSelectMultipleOrder = New System.Windows.Forms.Button()
        Me.fileDailog2 = New System.Windows.Forms.OpenFileDialog()
        Me.btnMultipleExecute = New System.Windows.Forms.Button()
        Me.lblCSVPath = New System.Windows.Forms.Label()
        Me.lblOrderPath = New System.Windows.Forms.Label()
        Me.lblDestinationPath = New System.Windows.Forms.Label()
        Me.lblSourcePath = New System.Windows.Forms.Label()
        Me.lstTemp = New System.Windows.Forms.ListBox()
        Me.txtTempSelectedFile = New System.Windows.Forms.TextBox()
        Me.grpOption.SuspendLayout()
        Me.grpSingle.SuspendLayout()
        Me.grpMultiple.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExecute
        '
        Me.btnExecute.BackColor = System.Drawing.Color.LightGreen
        Me.btnExecute.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnExecute.Location = New System.Drawing.Point(24, 487)
        Me.btnExecute.Margin = New System.Windows.Forms.Padding(5)
        Me.btnExecute.Name = "btnExecute"
        Me.btnExecute.Size = New System.Drawing.Size(164, 57)
        Me.btnExecute.TabIndex = 15
        Me.btnExecute.Text = "Execute"
        Me.btnExecute.UseVisualStyleBackColor = False
        '
        'btnSelectSingleOrder
        '
        Me.btnSelectSingleOrder.BackColor = System.Drawing.Color.LightGreen
        Me.btnSelectSingleOrder.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnSelectSingleOrder.Location = New System.Drawing.Point(16, 70)
        Me.btnSelectSingleOrder.Margin = New System.Windows.Forms.Padding(5)
        Me.btnSelectSingleOrder.Name = "btnSelectSingleOrder"
        Me.btnSelectSingleOrder.Size = New System.Drawing.Size(105, 28)
        Me.btnSelectSingleOrder.TabIndex = 14
        Me.btnSelectSingleOrder.Text = "Select PO"
        Me.btnSelectSingleOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSelectSingleOrder.UseVisualStyleBackColor = False
        '
        'txtSelectedFile
        '
        Me.txtSelectedFile.BackColor = System.Drawing.Color.MistyRose
        Me.txtSelectedFile.Location = New System.Drawing.Point(132, 74)
        Me.txtSelectedFile.Margin = New System.Windows.Forms.Padding(5)
        Me.txtSelectedFile.Name = "txtSelectedFile"
        Me.txtSelectedFile.Size = New System.Drawing.Size(591, 22)
        Me.txtSelectedFile.TabIndex = 13
        '
        'cmbClient1
        '
        Me.cmbClient1.BackColor = System.Drawing.Color.MistyRose
        Me.cmbClient1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbClient1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmbClient1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClient1.FormattingEnabled = True
        Me.cmbClient1.Location = New System.Drawing.Point(132, 37)
        Me.cmbClient1.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbClient1.Name = "cmbClient1"
        Me.cmbClient1.Size = New System.Drawing.Size(227, 24)
        Me.cmbClient1.TabIndex = 12
        '
        'lblClient1
        '
        Me.lblClient1.AutoSize = True
        Me.lblClient1.Location = New System.Drawing.Point(39, 40)
        Me.lblClient1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblClient1.Name = "lblClient1"
        Me.lblClient1.Size = New System.Drawing.Size(82, 16)
        Me.lblClient1.TabIndex = 11
        Me.lblClient1.Text = "Select Client"
        Me.lblClient1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'fileDailog1
        '
        Me.fileDailog1.FileName = "fileDailog1"
        '
        'rdSingle
        '
        Me.rdSingle.AutoSize = True
        Me.rdSingle.Checked = True
        Me.rdSingle.Location = New System.Drawing.Point(34, 24)
        Me.rdSingle.Name = "rdSingle"
        Me.rdSingle.Size = New System.Drawing.Size(89, 20)
        Me.rdSingle.TabIndex = 16
        Me.rdSingle.TabStop = True
        Me.rdSingle.Text = "Single File"
        Me.rdSingle.UseVisualStyleBackColor = True
        '
        'rdMultiple
        '
        Me.rdMultiple.AutoSize = True
        Me.rdMultiple.Location = New System.Drawing.Point(155, 24)
        Me.rdMultiple.Name = "rdMultiple"
        Me.rdMultiple.Size = New System.Drawing.Size(104, 20)
        Me.rdMultiple.TabIndex = 17
        Me.rdMultiple.Text = "Multiple Files"
        Me.rdMultiple.UseVisualStyleBackColor = True
        '
        'grpOption
        '
        Me.grpOption.Controls.Add(Me.rdSingle)
        Me.grpOption.Controls.Add(Me.rdMultiple)
        Me.grpOption.Location = New System.Drawing.Point(24, 20)
        Me.grpOption.Name = "grpOption"
        Me.grpOption.Size = New System.Drawing.Size(294, 59)
        Me.grpOption.TabIndex = 20
        Me.grpOption.TabStop = False
        Me.grpOption.Text = "Choose Below Option"
        '
        'grpSingle
        '
        Me.grpSingle.Controls.Add(Me.cmbClient1)
        Me.grpSingle.Controls.Add(Me.lblClient1)
        Me.grpSingle.Controls.Add(Me.btnSelectSingleOrder)
        Me.grpSingle.Controls.Add(Me.txtSelectedFile)
        Me.grpSingle.Location = New System.Drawing.Point(24, 97)
        Me.grpSingle.Name = "grpSingle"
        Me.grpSingle.Size = New System.Drawing.Size(740, 120)
        Me.grpSingle.TabIndex = 21
        Me.grpSingle.TabStop = False
        Me.grpSingle.Text = "Single File Selection"
        '
        'grpMultiple
        '
        Me.grpMultiple.Controls.Add(Me.lblFileCount)
        Me.grpMultiple.Controls.Add(Me.lstFiles)
        Me.grpMultiple.Controls.Add(Me.cmbClient2)
        Me.grpMultiple.Controls.Add(Me.lblClient2)
        Me.grpMultiple.Controls.Add(Me.btnSelectMultipleOrder)
        Me.grpMultiple.Location = New System.Drawing.Point(24, 239)
        Me.grpMultiple.Name = "grpMultiple"
        Me.grpMultiple.Size = New System.Drawing.Size(740, 199)
        Me.grpMultiple.TabIndex = 22
        Me.grpMultiple.TabStop = False
        Me.grpMultiple.Text = "Multiple File Selection"
        '
        'lblFileCount
        '
        Me.lblFileCount.AutoSize = True
        Me.lblFileCount.Location = New System.Drawing.Point(133, 169)
        Me.lblFileCount.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblFileCount.Name = "lblFileCount"
        Me.lblFileCount.Size = New System.Drawing.Size(0, 16)
        Me.lblFileCount.TabIndex = 28
        Me.lblFileCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lstFiles
        '
        Me.lstFiles.BackColor = System.Drawing.Color.MistyRose
        Me.lstFiles.FormattingEnabled = True
        Me.lstFiles.ItemHeight = 16
        Me.lstFiles.Location = New System.Drawing.Point(132, 73)
        Me.lstFiles.Name = "lstFiles"
        Me.lstFiles.Size = New System.Drawing.Size(591, 84)
        Me.lstFiles.TabIndex = 15
        '
        'cmbClient2
        '
        Me.cmbClient2.BackColor = System.Drawing.Color.MistyRose
        Me.cmbClient2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbClient2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmbClient2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClient2.FormattingEnabled = True
        Me.cmbClient2.Location = New System.Drawing.Point(132, 37)
        Me.cmbClient2.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbClient2.Name = "cmbClient2"
        Me.cmbClient2.Size = New System.Drawing.Size(227, 24)
        Me.cmbClient2.TabIndex = 12
        '
        'lblClient2
        '
        Me.lblClient2.AutoSize = True
        Me.lblClient2.Location = New System.Drawing.Point(39, 40)
        Me.lblClient2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblClient2.Name = "lblClient2"
        Me.lblClient2.Size = New System.Drawing.Size(82, 16)
        Me.lblClient2.TabIndex = 11
        Me.lblClient2.Text = "Select Client"
        Me.lblClient2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnSelectMultipleOrder
        '
        Me.btnSelectMultipleOrder.BackColor = System.Drawing.Color.LightGreen
        Me.btnSelectMultipleOrder.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnSelectMultipleOrder.Location = New System.Drawing.Point(16, 71)
        Me.btnSelectMultipleOrder.Margin = New System.Windows.Forms.Padding(5)
        Me.btnSelectMultipleOrder.Name = "btnSelectMultipleOrder"
        Me.btnSelectMultipleOrder.Size = New System.Drawing.Size(105, 28)
        Me.btnSelectMultipleOrder.TabIndex = 14
        Me.btnSelectMultipleOrder.Text = "Select PO(s)"
        Me.btnSelectMultipleOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSelectMultipleOrder.UseVisualStyleBackColor = False
        '
        'fileDailog2
        '
        Me.fileDailog2.FileName = "fileDailog2"
        Me.fileDailog2.Multiselect = True
        '
        'btnMultipleExecute
        '
        Me.btnMultipleExecute.BackColor = System.Drawing.Color.LightGreen
        Me.btnMultipleExecute.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnMultipleExecute.Location = New System.Drawing.Point(250, 487)
        Me.btnMultipleExecute.Margin = New System.Windows.Forms.Padding(5)
        Me.btnMultipleExecute.Name = "btnMultipleExecute"
        Me.btnMultipleExecute.Size = New System.Drawing.Size(164, 57)
        Me.btnMultipleExecute.TabIndex = 23
        Me.btnMultipleExecute.Text = "Execute"
        Me.btnMultipleExecute.UseVisualStyleBackColor = False
        '
        'lblCSVPath
        '
        Me.lblCSVPath.AutoSize = True
        Me.lblCSVPath.Location = New System.Drawing.Point(442, 61)
        Me.lblCSVPath.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblCSVPath.Name = "lblCSVPath"
        Me.lblCSVPath.Size = New System.Drawing.Size(212, 16)
        Me.lblCSVPath.TabIndex = 27
        Me.lblCSVPath.Text = "D:\Daily Order\COSTA\Order-CSV"
        Me.lblCSVPath.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOrderPath
        '
        Me.lblOrderPath.AutoSize = True
        Me.lblOrderPath.Location = New System.Drawing.Point(442, 32)
        Me.lblOrderPath.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblOrderPath.Name = "lblOrderPath"
        Me.lblOrderPath.Size = New System.Drawing.Size(213, 16)
        Me.lblOrderPath.TabIndex = 26
        Me.lblOrderPath.Text = "D:\Daily Order\COSTA\Order-SRC"
        Me.lblOrderPath.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDestinationPath
        '
        Me.lblDestinationPath.AutoSize = True
        Me.lblDestinationPath.Location = New System.Drawing.Point(332, 61)
        Me.lblDestinationPath.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblDestinationPath.Name = "lblDestinationPath"
        Me.lblDestinationPath.Size = New System.Drawing.Size(111, 16)
        Me.lblDestinationPath.TabIndex = 25
        Me.lblDestinationPath.Text = "Destination Path :"
        Me.lblDestinationPath.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSourcePath
        '
        Me.lblSourcePath.AutoSize = True
        Me.lblSourcePath.Location = New System.Drawing.Point(356, 32)
        Me.lblSourcePath.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblSourcePath.Name = "lblSourcePath"
        Me.lblSourcePath.Size = New System.Drawing.Size(87, 16)
        Me.lblSourcePath.TabIndex = 24
        Me.lblSourcePath.Text = "Source Path :"
        Me.lblSourcePath.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lstTemp
        '
        Me.lstTemp.BackColor = System.Drawing.Color.MistyRose
        Me.lstTemp.FormattingEnabled = True
        Me.lstTemp.ItemHeight = 16
        Me.lstTemp.Location = New System.Drawing.Point(598, 524)
        Me.lstTemp.Name = "lstTemp"
        Me.lstTemp.Size = New System.Drawing.Size(85, 20)
        Me.lstTemp.TabIndex = 28
        Me.lstTemp.Visible = False
        '
        'txtTempSelectedFile
        '
        Me.txtTempSelectedFile.BackColor = System.Drawing.Color.MistyRose
        Me.txtTempSelectedFile.Location = New System.Drawing.Point(598, 487)
        Me.txtTempSelectedFile.Margin = New System.Windows.Forms.Padding(5)
        Me.txtTempSelectedFile.Name = "txtTempSelectedFile"
        Me.txtTempSelectedFile.Size = New System.Drawing.Size(85, 22)
        Me.txtTempSelectedFile.TabIndex = 15
        Me.txtTempSelectedFile.Visible = False
        '
        'frmPDF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.MediumTurquoise
        Me.ClientSize = New System.Drawing.Size(815, 581)
        Me.Controls.Add(Me.txtTempSelectedFile)
        Me.Controls.Add(Me.lstTemp)
        Me.Controls.Add(Me.lblCSVPath)
        Me.Controls.Add(Me.lblOrderPath)
        Me.Controls.Add(Me.lblDestinationPath)
        Me.Controls.Add(Me.lblSourcePath)
        Me.Controls.Add(Me.grpMultiple)
        Me.Controls.Add(Me.btnMultipleExecute)
        Me.Controls.Add(Me.grpSingle)
        Me.Controls.Add(Me.grpOption)
        Me.Controls.Add(Me.btnExecute)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPDF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BHG IT PO Quantity Filler"
        Me.grpOption.ResumeLayout(False)
        Me.grpOption.PerformLayout()
        Me.grpSingle.ResumeLayout(False)
        Me.grpSingle.PerformLayout()
        Me.grpMultiple.ResumeLayout(False)
        Me.grpMultiple.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents btnExecute As Button
    Private WithEvents btnSelectSingleOrder As Button
    Private WithEvents txtSelectedFile As TextBox
    Private WithEvents cmbClient1 As ComboBox
    Private WithEvents lblClient1 As Label
    Friend WithEvents fileDailog1 As OpenFileDialog
    Friend WithEvents rdSingle As RadioButton
    Friend WithEvents rdMultiple As RadioButton
    Friend WithEvents grpOption As GroupBox
    Friend WithEvents grpSingle As GroupBox
    Friend WithEvents grpMultiple As GroupBox
    Private WithEvents cmbClient2 As ComboBox
    Private WithEvents lblClient2 As Label
    Private WithEvents btnSelectMultipleOrder As Button
    Friend WithEvents fileDailog2 As OpenFileDialog
    Friend WithEvents lstFiles As ListBox
    Private WithEvents btnMultipleExecute As Button
    Private WithEvents lblCSVPath As Label
    Private WithEvents lblOrderPath As Label
    Private WithEvents lblDestinationPath As Label
    Private WithEvents lblSourcePath As Label
    Friend WithEvents lstTemp As ListBox
    Private WithEvents txtTempSelectedFile As TextBox
    Private WithEvents lblFileCount As Label
End Class
