﻿Imports System.IO
Imports System.Text
Imports System.Xml
Imports excelDoc = Microsoft.Office.Interop.Excel

Public Class frmPDF
    'testing comment
    Dim initialDirectory As String
    Dim selectedFileDirectory As String
    Shared ordererCodeFromExcel As String
    Shared _generatedFileName As String

    Private Sub frmPDF_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fileDailog1 = New OpenFileDialog()
        fileDailog1.FileName = Nothing

        fileDailog2 = New OpenFileDialog()
        fileDailog2.FileName = Nothing

        initialDirectory = "D:\Daily Order\"
        selectedFileDirectory = ""
        cmbClient1.Items.Add("--Select--")
        cmbClient1.Items.Add("COSTA")
        cmbClient1.Items.Add("CARIBO")
        cmbClient1.SelectedIndex = 0

        cmbClient2.Items.Add("--Select--")
        cmbClient2.Items.Add("COSTA")
        cmbClient2.Items.Add("CARIBO")
        cmbClient2.SelectedIndex = 0

        btnSelectSingleOrder.Enabled = False
        btnSelectMultipleOrder.Enabled = False

        Me.Size = New Point(800, 420)
    End Sub

    Private Sub btnSelectOrder_Click(sender As Object, e As EventArgs) Handles btnSelectSingleOrder.Click
        Dim fName As String = String.Empty
        fileDailog1.InitialDirectory = initialDirectory
        fileDailog1.Title = "Select file to be upload."
        fileDailog1.Filter = "PDF Files (*.pdf)|*.pdf|" & "All Files (*.*)|*.*"
        fileDailog1.FilterIndex = 2
        fileDailog1.Multiselect = False
        fileDailog1.RestoreDirectory = True

        If fileDailog1.ShowDialog() = DialogResult.OK Then
            fName = fileDailog1.FileName
            selectedFileDirectory = Path.GetDirectoryName(fName.ToString())

            If Not fName.Contains(initialDirectory) Then
                MessageBox.Show("You can't select other folder to select a file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error)
                fileDailog1.FileName = String.Empty
                Me.txtSelectedFile.Text = String.Empty
                Me.txtTempSelectedFile.Text = String.Empty
                Return
            Else
                Me.txtSelectedFile.Text = Path.GetFileName(fName)
                Me.txtTempSelectedFile.Text = fName
                lblOrderPath.Text = Path.GetDirectoryName(fName.ToString())
                lblCSVPath.Text = Path.GetDirectoryName(fName.ToString()).Replace("Order-SRC", "Order-CSV")
            End If
            Me.btnExecute.Focus()

        End If

    End Sub

    Private Sub btnSelectMultipleOrder_Click(sender As Object, e As EventArgs) Handles btnSelectMultipleOrder.Click
        Dim fNames() As String = Nothing
        fileDailog2.InitialDirectory = initialDirectory
        fileDailog2.Title = "Select file to be upload."
        fileDailog2.Filter = "PDF Files (*.pdf)|*.pdf|" & "All Files (*.*)|*.*"
        fileDailog2.FilterIndex = 2
        fileDailog2.Multiselect = True
        fileDailog2.RestoreDirectory = True

        If lstTemp.Items.Count <> 0 Then
            selectedFileDirectory = Path.GetDirectoryName(lstTemp.Items(0).ToString())
        End If

        If fileDailog2.ShowDialog() = DialogResult.OK Then
            fNames = fileDailog2.FileNames
            If fNames.Count <> 0 Then
                For i As Integer = 0 To fNames.Count - 1
                    Dim currentFile As String = fNames(i).ToString()
                    If Not currentFile.Contains(initialDirectory) OrElse Not currentFile.Contains(selectedFileDirectory) Then
                        MessageBox.Show("You can't select other folder to select a file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error)

                        Return
                    Else
                        Me.lstFiles.Items.Add(Path.GetFileName(currentFile))
                        Me.lstTemp.Items.Add(currentFile)
                        lblOrderPath.Text = Path.GetDirectoryName(lstTemp.Items(0).ToString())
                        lblCSVPath.Text = Path.GetDirectoryName(lstTemp.Items(0).ToString()).Replace("Order-SRC", "Order-CSV")
                    End If
                Next
                lblFileCount.Text = lstTemp.Items.Count.ToString() + " Orders selected for execution."

                Me.btnMultipleExecute.Focus()
            End If

        End If

    End Sub

    Private Sub btnExecute_Click(sender As Object, e As EventArgs) Handles btnExecute.Click
        Try
            Cursor = Cursors.WaitCursor
            Application.DoEvents()

            Dim msgStr As StringBuilder = New StringBuilder()
            Dim filename As String = System.IO.Path.GetFileName(fileDailog1.FileName)
            Dim clientDirectory As String = String.Empty

            If cmbClient1.SelectedIndex = 0 Then
                msgStr.Append("- Please select client" & Environment.NewLine)
            End If

            If String.IsNullOrEmpty(filename) Then
                msgStr.Append("- Please select a Order")
            End If

            If (Not String.IsNullOrEmpty(txtTempSelectedFile.Text)) Then
                Dim selectedFileRoutePath = Path.GetDirectoryName(Path.GetDirectoryName(txtTempSelectedFile.Text))
                If Not selectedFileRoutePath Is Nothing AndAlso Not selectedFileRoutePath.Contains(cmbClient1.SelectedItem) Then
                    msgStr.Append("- Selected Order is not for selected client.")
                End If
            End If

            If msgStr.ToString() <> "" Then
                MessageBox.Show(msgStr.ToString(), "Validate below things.!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                clientDirectory = System.IO.Path.GetDirectoryName(fileDailog1.FileName)
                Dim retStatus As Boolean = UpdateDataAsPerTemplate(fileDailog1.FileName, clientDirectory)

                Dim statusMsg As String = String.Empty

                If retStatus Then

                    MessageBox.Show("File has been generated successfully.!!!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'Dim newFile As String = _generatedFileName
                    'Dim generatedFileName As String = clientDirectory.Replace("Order-SRC", "Order-CSV") & "\" & Path.GetFileNameWithoutExtension(filename) & ".csv"
                    Dim uploadedFileName As String = Path.GetFileName(filename)
                    Dim newFile As String = Path.GetFileNameWithoutExtension(_generatedFileName)

                    If uploadedFileName <> newFile AndAlso ordererCodeFromExcel <> String.Empty AndAlso ordererCodeFromExcel <> Path.GetFileNameWithoutExtension(filename) Then
                        My.Computer.FileSystem.RenameFile(clientDirectory + "\" + filename, newFile + ".pdf")
                    End If

                    System.Diagnostics.Process.Start(_generatedFileName)
                Else
                    'MessageBox.Show("File is not generated, Some error while process, Please check error log.!!!", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If

            ResetControls()
            Cursor = Cursors.Default

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Cursor = Cursors.Default
        End Try
    End Sub


    Private Sub btnMultipleExecute_Click(sender As Object, e As EventArgs) Handles btnMultipleExecute.Click
        Try
            Cursor = Cursors.WaitCursor
            Application.DoEvents()

            Dim succeedFiles As Integer = 0
            Dim failedFiles As Integer = 0
            Dim msgStr As StringBuilder = New StringBuilder()
            Dim filenames() As String = lstFiles.Items.Cast(Of Object).Select(Function(o) System.IO.Path.GetFileName(lstFiles.GetItemText(o))).ToArray
            Dim clientDirectory As String = String.Empty

            If cmbClient2.SelectedIndex = 0 Then
                msgStr.Append("- Please select client" & Environment.NewLine)
            End If

            If filenames.Count() = 0 Then
                msgStr.Append("- Please select a Order")
            End If

            For i As Integer = 0 To filenames.Count - 1
                Dim selectedFile As String = lstTemp.Items(i).ToString()
                If (Not String.IsNullOrEmpty(selectedFile)) Then
                    Dim selectedFileRoutePath = Path.GetDirectoryName(Path.GetDirectoryName(selectedFile))
                    If Not selectedFileRoutePath Is Nothing AndAlso Not selectedFileRoutePath.Contains(cmbClient2.SelectedItem) Then
                        msgStr.Append("- Selected Order is not for selected client.")
                    End If
                End If
            Next

            If msgStr.ToString() <> "" Then
                MessageBox.Show(msgStr.ToString(), "Validate below things.!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                For j As Integer = 0 To filenames.Count - 1
                    Dim currentFile As String = lstTemp.Items(j).ToString()
                    clientDirectory = System.IO.Path.GetDirectoryName(currentFile)

                    Dim retStatus As Boolean = UpdateDataAsPerTemplate(currentFile, clientDirectory)

                    Dim statusMsg As String = String.Empty

                    If retStatus Then
                        'Dim generatedFileName As String = clientDirectory.Replace("Order-SRC", "Order-CSV") & "\" & Path.GetFileNameWithoutExtension(currentFile) & ".csv"
                        Dim uploadedFileName As String = Path.GetFileName(currentFile)
                        Dim newFile As String = Path.GetFileNameWithoutExtension(_generatedFileName)

                        If uploadedFileName <> newFile AndAlso ordererCodeFromExcel <> String.Empty AndAlso ordererCodeFromExcel <> Path.GetFileNameWithoutExtension(currentFile) Then
                            My.Computer.FileSystem.RenameFile(clientDirectory + "\" + uploadedFileName, newFile + ".pdf")
                        End If

                        succeedFiles = succeedFiles + 1

                    Else
                        'MessageBox.Show(currentFile + " : File is not generated, Some error while process, Please check error log.!!!", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        failedFiles = failedFiles + 1
                    End If
                Next
            End If

            If msgStr.ToString() = "" Then
                MessageBox.Show(succeedFiles.ToString() + " File(s) has been generated successfully.!!!" + Environment.NewLine, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                System.Diagnostics.Process.Start("explorer.exe", lblCSVPath.Text)
                ResetControls()
            End If

            Cursor = Cursors.Default

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Function GetTemplatePath() As String
        If cmbClient1.SelectedIndex = 1 Then
            Return "D:\Daily Order\COSTA"
        ElseIf cmbClient1.SelectedIndex = 2 Then
            Return "D:\Daily Order\CARIBO"
        Else
            Return "D:\Daily Order\COSTA"
        End If
    End Function

    Public Shared Function UpdateDataAsPerTemplate(ByVal fileName As String, ByVal clientDirectory As String) As Boolean
        Try
            Dim pdfFile As String = fileName
            Dim ordererFromPDF As String = ""

            Dim excelFile As String = Path.ChangeExtension(pdfFile, ".csv")
            Dim xmlFile As String = Path.ChangeExtension(pdfFile, ".xml")
            Dim xml As String = Nothing
            Dim generatedFile As String = String.Empty
            Dim f As SautinSoft.PdfFocus = New SautinSoft.PdfFocus()
            f.OpenPdf(pdfFile)

            If f.PageCount > 0 Then
                xml = f.ToXml()
                f.ClosePdf()
                Dim dtFromPDF As DataTable = New DataTable()
                Dim dtDestination As DataTable = New DataTable()

                If Not String.IsNullOrEmpty(xml) Then
                    File.WriteAllText(xmlFile, xml)
                    Dim fs As FileStream = New FileStream(xmlFile, FileMode.Open)
                    Dim xr As XmlTextReader = New XmlTextReader(fs)
                    Dim ds As DataSet = New DataSet()
                    ds.ReadXml(xr)
                    dtFromPDF = ds.Tables(1)
                    fs.Close()

                    If System.IO.File.Exists(xmlFile) Then System.IO.File.Delete(xmlFile)
                    Dim filteredRows As DataRow() = dtFromPDF.Select("row_Id = 12").AsEnumerable()
                    ordererFromPDF = filteredRows(2)("cell_Text")

                    Dim dtFromOrdererTemplate = GetOrdererTemplateData(clientDirectory)
                    Try
                        ordererCodeFromExcel = dtFromOrdererTemplate.Select("OrdererName = '" + ordererFromPDF.Trim() + "'").FirstOrDefault().Item("OrdererCode").ToString()
                    Catch ex As Exception
                        ordererCodeFromExcel = String.Empty
                        AddLog("ERROR : PDF orderer name is not found in excel for file : " + fileName + Environment.NewLine + ex.ToString())
                        MessageBox.Show("PDF orderer name is not found in excel for file :" + fileName + "." + Environment.NewLine + Environment.NewLine + "File will be saved without client code.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        'Return False
                    End Try

                    dtDestination = ExtractDataIntoDataTable(dtFromPDF)

                    generatedFile = CompareAndUpdateData(dtDestination, clientDirectory, Path.GetFileNameWithoutExtension(fileName), ordererCodeFromExcel)
                    Return True
                Else
                    AddLog("There is some issue while generating XML file")
                    Return False
                End If
            Else
                AddLog("There is some issue while opening pdf file")
                Return False
            End If

        Catch ex As Exception
            AddLog("ERROR : " & ex.ToString())
            Return False
        End Try
    End Function

    Private Shared Function ExtractDataIntoDataTable(ByVal dtFromPDF As DataTable) As DataTable
        Dim dtDestination As New DataTable
        Dim col2Added As Boolean = False
        Dim col3Added As Boolean = False
        Dim col4Added As Boolean = False
        Dim col5Added As Boolean = False
        Dim col6Added As Boolean = False

        Dim prevRowId As Integer = 0
        Dim dtNewRow As DataRow

        For index As Integer = 0 To dtFromPDF.Rows.Count - 1
            If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("row_id").ToString()) Then
                Dim rowId As Integer = dtFromPDF.Rows(index)("row_id").ToString()

                If index = 0 Then
                    prevRowId = 0
                Else
                    prevRowId = dtFromPDF.Rows(index - 1)("row_id").ToString()
                End If


                If rowId = 20 Then
                    Console.WriteLine(dtFromPDF.Rows(index)("cell_Text").ToString())
                    If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) Then
                        dtDestination.Columns.Add(dtFromPDF.Rows(index)("cell_Text").ToString())
                    End If
                ElseIf rowId > 20 And rowId < 200 Then

                    If prevRowId <> rowId Then
                        dtNewRow = dtDestination.NewRow()
                        col2Added = False
                        col3Added = False
                        col4Added = False
                        col5Added = False
                        col6Added = False

                        If dtFromPDF.Rows(index)("colspan").ToString() = "2" Then
                            dtNewRow(dtDestination.Columns(0).ColumnName) = ""
                        Else
                            If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) Then
                                dtNewRow(dtDestination.Columns(0).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            End If
                        End If
                    Else

                        If Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col2Added = False Then
                            dtNewRow(dtDestination.Columns(1).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col2Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col3Added = False Then
                            dtNewRow(dtDestination.Columns(2).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col3Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col4Added = False Then
                            dtNewRow(dtDestination.Columns(3).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col4Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col5Added = False Then
                            dtNewRow(dtDestination.Columns(4).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col5Added = True
                        ElseIf Not String.IsNullOrEmpty(dtFromPDF.Rows(index)("cell_Text").ToString()) AndAlso col6Added = False Then
                            dtNewRow(dtDestination.Columns(5).ColumnName) = dtFromPDF.Rows(index)("cell_Text").ToString()
                            col6Added = True
                        End If

                        If col2Added AndAlso col3Added AndAlso col4Added AndAlso col5Added AndAlso col6Added Then
                            dtDestination.Rows.Add(dtNewRow)
                            col2Added = False
                            col3Added = False
                            col4Added = False
                            col5Added = False
                            col6Added = False
                        End If
                    End If

                End If
            End If

        Next

        Dim filter As String = "Item <> 'Please note that prices exclude VAT'"
        Dim FilteredRows As DataRow() = dtDestination.Select(filter)
        dtDestination = FilteredRows.CopyToDataTable()

        Return dtDestination

    End Function

    Private Shared Function CompareAndUpdateData(ByVal dt As DataTable, ByVal clientDirectory As String, ByVal pdfFileName As String, ByVal fileName As String) As String
        Dim xlApp As excelDoc.Application = New excelDoc.Application()
        Dim xlWorkBook As excelDoc.Workbook
        Dim xlWorkSheet As excelDoc.Worksheet
        Dim selectedFilePath As String = clientDirectory
        Dim destinationFilePath As String
        Dim templateFilePath As String
        If xlApp Is Nothing Then
            AddLog("Excel is not properly installed!!")
            Return String.Empty
        Else
            templateFilePath = Path.GetDirectoryName(clientDirectory)
            templateFilePath = templateFilePath.Replace("Order-SRC", "Template-XLS")
            'Dim templateFileName = GetTemplateFileName(templateFilePath + "\")
            xlApp.DisplayAlerts = False

            xlWorkBook = xlApp.Workbooks.Open(templateFilePath + "\" + "Template.csv")
            xlWorkSheet = xlWorkBook.Sheets(1)
            Dim iRow As Integer = 0

            For iRow = 1 To xlWorkSheet.Rows.Count
                Dim destCode As String = xlWorkSheet.Cells(iRow, 1).value
                Dim destQuntity As String = xlWorkSheet.Cells(iRow, 3).value

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim quant As Double
                    Dim sourceCode As String = dt.Rows(i)(0).ToString()

                    If Not String.IsNullOrEmpty(sourceCode) Then

                        If sourceCode = destCode Then
                            quant = System.Convert.ToDouble(dt.Rows(i)(2).ToString())
                            xlWorkSheet.Cells(iRow, 3).value = quant
                        End If
                    End If
                Next

                If (xlWorkSheet.Cells(iRow, 1).value) = "" Then Exit For
                If String.IsNullOrEmpty(fileName) Then
                    'xlWorkSheet.Cells(iRow, 4).value = pdfFileName
                Else
                    xlWorkSheet.Cells(iRow, 4).value = fileName
                End If

            Next
        End If

        Dim misValue As Object = System.Reflection.Missing.Value

        destinationFilePath = clientDirectory.Replace("Order-SRC", "Order-CSV")
        If Not Directory.Exists(destinationFilePath & "\") Then
            Directory.CreateDirectory(destinationFilePath & "\")
        End If

        Dim generatedFileName As String
        Dim generatedFileNameWithoutExtension As String
        If String.IsNullOrEmpty(fileName) Then
            generatedFileName = destinationFilePath & "\" & pdfFileName & ".csv"
            generatedFileNameWithoutExtension = destinationFilePath & "\" & pdfFileName
        Else
            generatedFileName = destinationFilePath & "\" & fileName & ".csv"
            generatedFileNameWithoutExtension = destinationFilePath & "\" & fileName
        End If

        Dim newFileName As String
        Dim n As Integer = 1

        While (File.Exists(generatedFileName))
            newFileName = String.Format("{0}-{1}", generatedFileNameWithoutExtension, n)
            generatedFileName = newFileName + ".csv"
            n = n + 1
        End While

        _generatedFileName = generatedFileName
        xlWorkBook.SaveAs(generatedFileName, excelDoc.XlFileFormat.xlCSVWindows, misValue, misValue, misValue, misValue, excelDoc.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
        xlApp.Quit()
        ReleaseObject(xlApp)
        ReleaseObject(xlWorkBook)
        ReleaseObject(xlWorkSheet)
        Return generatedFileName
    End Function

    Private Shared Function GetOrdererTemplateData(ByVal clientDirectory As String) As DataTable
        Dim dtRows As DataTable = New DataTable()
        dtRows.Columns.Add("OrdererCode", System.Type.GetType("System.String"))
        dtRows.Columns.Add("OrdererName", System.Type.GetType("System.String"))

        Dim xlApp As excelDoc.Application = New excelDoc.Application()
        Dim xlWorkBook As excelDoc.Workbook
        Dim xlWorkSheet As excelDoc.Worksheet
        Dim templateFilePath As String
        If xlApp Is Nothing Then
            AddLog("Excel is not properly installed!!")
            Return Nothing
        Else
            Try
                templateFilePath = Path.GetDirectoryName(clientDirectory)
                templateFilePath = templateFilePath.Replace("Order-SRC", "Template-XLS")

                xlApp.DisplayAlerts = False

                xlWorkBook = xlApp.Workbooks.Open(templateFilePath + "\" + "OrdererTemplate.xlsx")
                xlWorkSheet = xlWorkBook.Sheets(1)
                Dim iRow As Integer = 0

                For iRow = 1 To xlWorkSheet.Rows.Count
                    Dim code As String = xlWorkSheet.Cells(iRow, 1).value
                    Dim ordererName As String = xlWorkSheet.Cells(iRow, 2).value
                    dtRows.Rows.Add(code, ordererName)

                    If (xlWorkSheet.Cells(iRow, 1).value) = "" Then Exit For
                Next
            Catch ex As Exception

            End Try
        End If

        xlApp.Quit()
        ReleaseObject(xlApp)
        ReleaseObject(xlWorkBook)
        ReleaseObject(xlWorkSheet)
        Return dtRows
    End Function


    Private Shared Function GetTemplateFileName(ByVal directoryPath As String) As String
        Dim templateFile As String = ""
        Dim di As DirectoryInfo = New DirectoryInfo(directoryPath)
        Dim firstFileName As String = di.GetFiles().[Select](Function(fi) fi.Name).FirstOrDefault()

        If firstFileName IsNot Nothing Then
            templateFile = directoryPath & firstFileName
        End If

        Return templateFile
    End Function

    Private Shared Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Public Shared Sub AddLog(ByVal errorMessage As String)
        Dim traceWriter As System.Diagnostics.TextWriterTraceListener = Nothing
        Dim stringBuilder As System.Text.StringBuilder = Nothing

        Try
            stringBuilder = New System.Text.StringBuilder()

            'Dim appPath As String = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Application.ExecutablePath)))
            'Dim appPath As String = Path.GetDirectoryName(Environment.CurrentDirectory)
            stringBuilder.Append("D:\PDFExtractor" & "\Log\")
            stringBuilder.Append(DateTime.Now.Year.ToString())
            stringBuilder.Append("\")
            stringBuilder.Append(DateTime.Now.Month.ToString())
            stringBuilder.Append("\")
            stringBuilder.Append(DateTime.Now.Day.ToString())

            If (Not Directory.Exists(stringBuilder.ToString())) Then
                Directory.CreateDirectory(stringBuilder.ToString())
            End If

            stringBuilder.Append("\Error.log")
            traceWriter = New System.Diagnostics.TextWriterTraceListener(stringBuilder.ToString())
            stringBuilder = New System.Text.StringBuilder()

            stringBuilder.Append(vbCrLf)
            stringBuilder.Append("Date : ")
            stringBuilder.Append(DateTime.Now.ToString())

            stringBuilder.Append(vbCrLf)
            stringBuilder.Append("Error : ")
            stringBuilder.Append(errorMessage)

            stringBuilder.Append(vbCrLf)
            traceWriter.WriteLine(stringBuilder.ToString())
        Catch
        Finally

            If (traceWriter IsNot Nothing) Then
                traceWriter.Close()
                traceWriter.Dispose()
            End If

            stringBuilder = Nothing
        End Try
    End Sub

    Private Sub cmbClient1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbClient1.SelectedIndexChanged
        If cmbClient1.SelectedIndex = 1 Then
            btnSelectSingleOrder.Enabled = True
            ShowPaths(True)
            initialDirectory = "D:\Daily Order\COSTA\Order-SRC\"
            lblOrderPath.Text = "D:\Daily Order\COSTA\Order-SRC\"
            lblCSVPath.Text = "D:\Daily Order\COSTA\Order-CSV\"
        ElseIf cmbClient1.SelectedIndex = 2 Then
            btnSelectSingleOrder.Enabled = True
            ShowPaths(True)
            initialDirectory = "D:\Daily Order\CARIBO\Order-SRC\"
            lblOrderPath.Text = "D:\Daily Order\CARIBO\Order-SRC\"
            lblCSVPath.Text = "D:\Daily Order\CARIBO\Order-CSV\"
        Else
            initialDirectory = "D:\Daily Order\"
            btnSelectSingleOrder.Enabled = False
            ShowPaths(False)
        End If
    End Sub

    Private Sub cmbClient2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbClient2.SelectedIndexChanged
        If cmbClient2.SelectedIndex = 1 Then
            btnSelectMultipleOrder.Enabled = True
            ShowPaths(True)
            initialDirectory = "D:\Daily Order\COSTA\Order-SRC\"
            lblOrderPath.Text = "D:\Daily Order\COSTA\Order-SRC\"
            lblCSVPath.Text = "D:\Daily Order\COSTA\Order-CSV\"
        ElseIf cmbClient2.SelectedIndex = 2 Then
            btnSelectMultipleOrder.Enabled = True
            ShowPaths(True)
            initialDirectory = "D:\Daily Order\CARIBO\Order-SRC\"
            lblOrderPath.Text = "D:\Daily Order\CARIBO\Order-SRC\"
            lblCSVPath.Text = "D:\Daily Order\CARIBO\Order-CSV\"
        Else
            initialDirectory = "D:\Daily Order\"
            btnSelectMultipleOrder.Enabled = False
            ShowPaths(False)
        End If
    End Sub

    Private Sub rdSingle_CheckedChanged(sender As Object, e As EventArgs) Handles rdSingle.CheckedChanged
        If rdSingle.Checked Then
            grpSingle.Visible = True
            grpMultiple.Visible = False
            grpSingle.Location = New Point(24, 97)

            btnExecute.Visible = True
            btnMultipleExecute.Visible = False
            btnExecute.Location = New Point(320, 240)

            ResetControls()
        End If
    End Sub

    Private Sub rdMultiple_CheckedChanged(sender As Object, e As EventArgs) Handles rdMultiple.CheckedChanged
        If rdMultiple.Checked Then
            grpMultiple.Visible = True
            grpSingle.Visible = False
            grpMultiple.Location = New Point(24, 97)

            btnMultipleExecute.Visible = True
            btnExecute.Visible = False
            btnMultipleExecute.Location = New Point(320, 310)

            ResetControls()
        End If
    End Sub

    Private Sub ResetControls()

        If cmbClient1 IsNot Nothing AndAlso cmbClient1.Items.Count > 0 Then
            cmbClient1.SelectedIndex = 0
        End If

        If cmbClient2 IsNot Nothing AndAlso cmbClient2.Items.Count > 0 Then
            cmbClient2.SelectedIndex = 0
        End If

        txtSelectedFile.Text = ""
        txtTempSelectedFile.Text = ""
        selectedFileDirectory = ""
        lblFileCount.Text = ""
        lstFiles.Items.Clear()
        lstTemp.Items.Clear()
    End Sub

    Private Sub ShowPaths(ByVal isShow As Boolean)
        lblSourcePath.Visible = isShow
        lblDestinationPath.Visible = isShow
        lblOrderPath.Visible = isShow
        lblCSVPath.Visible = isShow
    End Sub
End Class